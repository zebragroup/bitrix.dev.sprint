<?php

namespace Sprint;

class Bitrix {

    /* @return \CMain */
    public static function getApplication(){
        return $GLOBALS['APPLICATION'];
    }

    /* @return \CUser */
    public static function getUser(){
        return $GLOBALS['USER'];
    }

    /* @return \CDatabase */
    public static function getDb(){
        return $GLOBALS['DB'];
    }
}