<?php

namespace Sprint\Forms\Fields;

use Sprint\Forms\Field;

class ConfirmTerms extends Field{

    protected function initialize(){
        $this->setTemplate('confirm_terms', array('message'));
    }

	protected function bindValue($value){
		return $value;
	}

}

