<?php

namespace Sprint\Forms\Validators;
use Sprint\Forms\Validator;

class ConfirmTerms extends Validator {

    public function isValid($value){
		return ($value == 'Y');
    }
}

