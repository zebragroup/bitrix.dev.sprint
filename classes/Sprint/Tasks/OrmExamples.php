<?php

namespace Sprint\Tasks;
use Sprint\Task;

use Sprint\IblockHelper;

class OrmExamples extends Task {

    public function executeCreateIblock(){

        $helper = new IblockHelper();

        $helper->addIblockTypeIfNotExists(array(
            'ID' => 'orm_examples_type',
            'LANG'=>Array(
                'en'=>Array(
                    'NAME'=>'orm_examples',
                    'SECTION_NAME'=>'Sections',
                    'ELEMENT_NAME'=>'Elements'
                ),
                'ru'=>Array(
                    'NAME'=>'Example ',
                    'SECTION_NAME'=>'Разделы',
                    'ELEMENT_NAME'=>'Элементы'
                ),
            )
        ));

        $ormId = $helper->addIblockIfNotExists(array(
            'NAME' => 'orm_examples',
            'CODE' => 'orm_examples',
            'IBLOCK_TYPE_ID' => 'orm_examples_type',
            'LIST_PAGE_URL' => '',
            'DETAIL_PAGE_URL' => '/examples/#ELEMENT_ID#'
        ));

        $ormLinkedId = $helper->addIblockIfNotExists(array(
            'NAME' => 'orm_examples_linked',
            'CODE' => 'orm_examples_linked',
            'IBLOCK_TYPE_ID' => 'orm_examples_type',
            'LIST_PAGE_URL' => '',
            'DETAIL_PAGE_URL' => '/examples/linked/#ELEMENT_ID#'
        ));
        
        
        
        $helper->addPropertyIfNotExists($ormId, array(
            'NAME' => 'STR_ONE',
            'CODE' => 'STR_ONE',
            'PROPERTY_TYPE' => 'S',
			'WITH_DESCRIPTION' => 'Y',
        ));
        $helper->addPropertyIfNotExists($ormId, array(
            'NAME' => 'STR_MANY',
            'CODE' => 'STR_MANY',
            'PROPERTY_TYPE' => 'S',
            'MULTIPLE' => 'Y',
			'WITH_DESCRIPTION' => 'Y',
        ));
        
        $helper->addPropertyIfNotExists($ormId, array(
            'NAME' => "LIST_ONE",
            'CODE' => 'LIST_ONE',
            'VALUES' => array(
                array('XML_ID' => 'var1','VALUE' => 'val1', 'DEF' => 'N', 'SORT' => 100),
                array('XML_ID' => 'var2','VALUE' => 'val2', 'DEF' => 'N', 'SORT' => 110),
                array('XML_ID' => 'var3','VALUE' => 'val3', 'DEF' => 'N', 'SORT' => 110),
            )));

        $helper->addPropertyIfNotExists($ormId, array(
            'NAME' => "LIST_MANY",
            'CODE' => 'LIST_MANY',
            'MULTIPLE' => 'Y',
            'VALUES' => array(
                array('XML_ID' => 'var1','VALUE' => 'val1', 'DEF' => 'N', 'SORT' => 100),
                array('XML_ID' => 'var2','VALUE' => 'val2', 'DEF' => 'N', 'SORT' => 110),
                array('XML_ID' => 'var3','VALUE' => 'val3', 'DEF' => 'N', 'SORT' => 110),
            )));


        $helper->addPropertyIfNotExists($ormId, array(
            'NAME' => 'LINKED_ONE',
            'CODE' => 'LINKED_ONE',
            'LINK_IBLOCK_ID' => $ormLinkedId,
            'USER_TYPE' => 'EList'
        ));

        $helper->addPropertyIfNotExists($ormId, array(
            'NAME' => 'LINKED_MANY',
            'CODE' => 'LINKED_MANY',
            'LINK_IBLOCK_ID' => $ormLinkedId,
            'USER_TYPE' => 'EList',
            'MULTIPLE' => 'Y',
        ));

        $helper->addPropertyIfNotExists($ormId, array(
            'NAME' => "FILES_ONE",
            'CODE' => 'FILES_ONE',
            'FILTRABLE' => 'N',
            'PROPERTY_TYPE' => 'F',
            'WITH_DESCRIPTION' => 'Y',
            'FILE_TYPE' => 'jpg, gif, bmp, png, jpeg'
        ));

        $helper->addPropertyIfNotExists($ormId, array(
            'NAME' => "FILES_MANY",
            'CODE' => 'FILES_MANY',
            'FILTRABLE' => 'N',
            'PROPERTY_TYPE' => 'F',
            'WITH_DESCRIPTION' => 'Y',
            'FILE_TYPE' => 'jpg, gif, bmp, png, jpeg',
            'MULTIPLE' => 'Y',
        ));
    }
} 