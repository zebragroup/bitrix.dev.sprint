<?php

if (!empty($_SERVER["HTTP_HOST"])){
    die('console only');
}

set_time_limit(0);

define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\CModule::IncludeModule('dev.sprint');
if (IsModuleInstalled('dev.sprint')){
    $manager = new Sprint\Console();
    $manager->executeFromArgs($argv);
}


require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
